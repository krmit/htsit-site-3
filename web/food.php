<!DOCTYPE html>
<html lang="sv">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>Matsedel - HTS IT</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

    <!-- SEO -->
    <meta name="description" content="Här kan du se veckans mat på på Hässleholms Tekniska Skola.">
    <meta property="og:description" content="Här kan du se veckans mat på på Hässleholms Tekniska Skola." />
    <meta property="og:title" content="Veckans mat på HTS" />
    <meta property="og:type" content="website" />
</head>
<body>
<header>
    <nav class="red lighten-2" role="navigation">
        <?php include_once('/modules/nav-content.php') ?>
    </nav>
</header>
<main>
    <div class="container">
        <div class="row">
            <div class="col s12 m8">
                <div class="section">
                    <h1 class="light">Matsedel</h1>
                    <?php include_once('/modules/foodmenu.php') ?>
                </div>
            </div>
            <div class="col s12 m4">
                <div class="section">
                    <?php include_once('/modules/quicklinks.php'); ?>
                </div>
                <div class="section">
                    <a class="twitter-timeline"  href="https://twitter.com/htsit3040" data-widget-id="630918607858495488">Tweets by @htsit3040</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include_once('/modules/footer.php'); ?>

<!--  Scripts-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="/js/materialize.js"></script>
<script src="/js/init.js"></script>

</body>
</html>
