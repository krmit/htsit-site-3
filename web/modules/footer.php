<footer class="page-footer red lighten-2">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Kort om oss</h5>
                <p class="grey-text text-lighten-4">
                    Vi är ett tekniskt program med inriktingen Informations- och medieteknik på
                    Hässleholms Tekniska Skola. Kort sagt, HTS IT. Är du intresserad av att
                    välja IT-inriktningen på HTS?
                </p>
                <a href="/about.php" class="btn white red-text text-lighten-2">Läs mer</a>
            </div>
            <div class="col l3 s12">
                <h5 class="white-text">Sociala medier</h5>
                <ul>
                    <li><a class="white-text" href="https://twitter.com/htsit3040" target="_blank">Twitter</a></li>
                </ul>
            </div>
            <div class="col l3 s12">
                <h5 class="white-text">Kontakt</h5>
                <ul class="white-text">
                    <li>Hässleholms Tekniska Skola</li>
                    <li>Stobygatan 7</li>
                    <li>281 39 Hässleholm </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col s6 left-align">
                    <p class="">Copyright &copy; <script>document.write(new Date().getFullYear())</script> HTS IT</p>
                </div>
                <div class="col s6 right-align">
                    <a href="#login-modal" class="white-text modal-trigger btn-flat waves-effect waves-light">Admin</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Login Modal -->
<div id="login-modal" class="modal">
    <div class="modal-content">
        <h4>Logga in som administratör</h4>
        <p class="flow-text">
            Här kan du som administratör med rättigheter logga in och ändra innehållet på sidan.
        </p>
        <div class="row">
            <form class="col s12" action="#" method="GET">
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Ditt användarnamn" id="login-user" name="user" type="text" class="validate">
                        <label for="login-user">Användarnamn</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Ditt lösenord" id="login-pass" name="pass" type="password" class="validate">
                        <label for="login-pass">Lösenord</label>
                    </div>
                </div>
                <button class="btn waves-effect waves-light red lighten-2 white-text" type="submit">
                    <i class="material-icons right">send</i>Logga in
                </button>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class=" modal-action modal-close waves-effect waves-red red-text text-lighten-2 btn-flat">Stäng</a>
    </div>
</div>