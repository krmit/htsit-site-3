<?php
require_once('simple_html_dom.php');
$content = file_get_html('http://hts.se/mat/');
$content = strip_tags($content, '<br>');
$items = explode('<br>', $content);
$weekNumber = date("W");
$food = array();
for($i = 2; $i < count($items); $i += 2)
{
    array_push($food, trim($items[$i]));
}

?>

<ul class="collection with-header z-depth-1">
    <li class="collection-header"><h3 class="light">Vecka <?php echo $weekNumber ?></h3></li>
    <li class="collection-header"><h5 class="light">Måndag</h5></li>
    <li class="collection-item"><?php echo $food[0] ?></li>
    <li class="collection-header"><h5 class="light">Tisdag</h5></li>
    <li class="collection-item"><?php echo $food[1] ?></li>
    <li class="collection-header"><h5 class="light">Onsdag</h5></li>
    <li class="collection-item"><?php echo $food[2] ?></li>
    <li class="collection-header"><h5 class="light">Torsdag</h5></li>
    <li class="collection-item"><?php echo $food[3] ?></li>
    <li class="collection-header"><h5 class="light">Fredag</h5></li>
    <li class="collection-item"><?php echo $food[4] ?></li>
</ul>

