<div class="nav-wrapper container">
    <a id="logo-container" href="/index.php" class="brand-logo light">Hässleholms Tekniska Skola &#8212; <b>IT</b></a>
    <ul class="right hide-on-med-and-down">
        <li><a href="/index.php">Hem</a></li>
        <li><a href="http://htsit.se/bb/">Forum</a></li>
        <li><a href="/courses.php">Kurser</a></li>
        <li><a href="/about.php">Om oss</a></li>
    </ul>

    <ul id="nav-mobile" class="side-nav">
        <li><a href="/index.php">Hem</a></li>
        <li><a href="http://htsit.se/bb/">Forum</a></li>
        <li><a href="/courses.php">Kurser</a></li>
        <li><a href="/about.php">Om oss</a></li>
    </ul>
    <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
</div>