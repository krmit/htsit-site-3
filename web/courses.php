<!DOCTYPE html>
<html lang="sv">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>Kurser - HTS IT</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

    <!-- SEO -->
    <meta name="description" content="Läs om de IT-specifika kurser som erbjuds på vårt program här på teknikprogrammet
    med inriktning informations- & medieteknik.">
    <meta property="og:description" content="Läs om de IT-specifika kurser som erbjuds på vårt program här på teknikprogrammet
    med inriktning informations- & medieteknik" />
    <meta property="og:title" content="Kurser på HTS IT" />
    <meta property="og:type" content="website" />
</head>
<body>
<header>
    <nav class="red lighten-2" role="navigation">
        <?php include_once('/modules/nav-content.php') ?>
    </nav>
</header>
<main>
    <div class="container">
        <div class="row">
            <div class="col s12 m8">
                <div class="section">
                    <h3 class="light">Kurser</h3>
                    <p>
                        Här finns en samling av de kurser som ges ut på IT-inriktningen här på HTS.
                    </p>
                    <div class="card cyan lighten-1">
                        <div class="card-content white-text">
                            <span class="card-title"><h5 class="header light"><i class="material-icons left">code</i>Programmering</h5></span>
                            <p>
                                Programmering är i visst avseende det som utgör grundverktyget för en modern IT-tekniker.
                                Kursen går igenom grunderna i Java och du lär dig skriva enklare program med ett
                                objektorienterat tillvägagångssätt.
                            </p>
                        </div>
                        <div class="card-action">
                            <a class="white-text" href="http://htsit.se/krmAcademy/Programing/">Läs mer</a>
                        </div>
                    </div>

                    <div class="card green lighten-1">
                        <div class="card-content white-text">
                            <span class="card-title"><h5 class="header light"><i class="material-icons left">language</i>Webbserverprogrammering</h5></span>
                            <p>
                                I den här kursen lär man sig skapa smarta hemsidor med dynamiskt innehåll.
                                Framförallt går kursen ut på att lära sig PHP och databaser, men ger även förståelse
                                för hur man kan använda olika CMS - exempelvis WordPress.
                            </p>
                        </div>
                        <div class="card-action">
                            <a class="white-text" href="http://htsit.se/krmAcademy/WebServer/">Läs mer</a>
                        </div>
                    </div>

                    <div class="card orange lighten-1">
                        <div class="card-content white-text">
                            <span class="card-title"><h5 class="header light"><i class="material-icons left">computer</i>Datorteknik</h5></span>
                            <p>
                                En väldigt nyttig kurs som ger en grundläggande förståelse för datorn och dess delar.
                                Kursen går även in på övriga datarelaterade ämnen som är ett måste för att fullt förstå
                                och utveckla för datorsystem.
                            </p>
                        </div>
                        <div class="card-action white-text">
                            <a class="white-text" href="http://htsit.se/krmAcademy/Computer/">Läs mer</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m4">
                <div class="section">
                    <?php include_once('/modules/quicklinks.php'); ?>
                </div>
                <div class="section">
                    <a class="twitter-timeline"  href="https://twitter.com/htsit3040" data-widget-id="630918607858495488">Tweets by @htsit3040</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include_once('/modules/footer.php'); ?>

<!--  Scripts-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="/js/materialize.js"></script>
<script src="/js/init.js"></script>

</body>
</html>
