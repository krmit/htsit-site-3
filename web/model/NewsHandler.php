<?php

namespace model;
use PDO;

/**
 * Class NewsHandler
 * Handles everything that has to do with news posts. Including updating and
 * retrieving from the database.
 */
final class NewsHandler
{
    /**
     * Gets all the posts from the database
     * @return array All news posts
     */
    public static function getAllPosts()
    {
        $posts = array();
        $db = Database::getDB();
        foreach($db->query('SELECT * FROM '.Database::TABLE_NEWS) as $row) {
            $writers = explode(';', $row["writers"]);
            $post = new NewsPost($row["id"], $row["title"], $row["content"], $row["postdate"], $row["lastedited"], $writers);
            $posts[] = $post;
        }
        return $posts;
    }

    /**
     * Adds the specified post to the database
     * @param NewsPost $post The post to add
     */
    public static function addPost(NewsPost $post)
    {
        $db = Database::getDB();
        $statement = $db->prepare("INSERT INTO " . Database::TABLE_NEWS . "(title, content, postdate, lastedited, writers)
        VALUES(:title, :content, :postdate, :lastedited, :writers)");
        $statement->bindParam(":title", $post->getTitle());
        $statement->bindParam(":content", $post->getContent());
        $statement->bindParam(":postdate", $post->getPostdate()->format(Util::DateTimeFormat));
        $statement->bindParam(":lastedited", $post->getLastEditedDate()->format(Util::DateTimeFormat));
        $writersString = "";
        foreach ($post->getWriters() as $writer) {
            $writersString .= $writer . ";";
        }
        $statement->bindParam(":writers", $writersString);
        $statement->execute();
    }

    /**
     * Deletes the news post with the given ID from the database
     * @param string $postId The id of the post to delete
     */
    public static function deletePost($postId)
    {
        $db = Database::getDB();
        $statement = $db->prepare("DELETE FROM " . Database::TABLE_NEWS . " WHERE id=:id");
        $statement->bindParam(":id", $postId);
    }

    /**
     * Update a given news post in the database
     * @param NewsPost $newPost The post to update
     */
    public static function updatePost(NewsPost $newPost)
    {
        $db = Database::getDB();
        $statement = $db->prepare("UPDATE " . Database::TABLE_NEWS . " SET title=:title, content=:content, lastedited=:lastedited, writers=:writers WHERE id=:id");
        $statement->bindParam(":id", $newPost->getId());
        $statement->bindParam(":title", $newPost->getTitle());
        $statement->bindParam(":content", $newPost->getContent());
        $statement->bindParam(":lastedited", $newPost->getLastEditedDate()->format(Util::DateTimeFormat));
        $writersString = "";
        foreach ($newPost->getWriters() as $writer) {
            $writersString .= $writer . ";";
        }
        $statement->bindParam(":writers", $writersString);
        $statement->execute();
    }

    /**
     * Get the total amount of news posts
     * @return int Total posts count
     */
    public static function getPostCount()
    {
        $db = Database::getDB();
        $statement = $db->query("SELECT * FROM " . Database::TABLE_NEWS);
        return $statement->rowCount();
    }

    /**
     * Get the latest n posts, as specified by the input parameter
     * @param int $count The amount of posts to get
     * @return array The n latest posts
     */
    public static function getLatestPosts($count)
    {
        $posts = array();
        $db = Database::getDB();
        $statement = $db->prepare("SELECT * FROM (SELECT * FROM " . Database::TABLE_NEWS . " ORDER BY id DESC LIMIT :count) sub ORDER BY id ASC");
        $statement->bindParam(":count", $count);
        $statement->execute();
        foreach ($statement->fetchAll(PDO::FETCH_ASSOC) as $row)
        {
            $writers = explode(';', $row["writers"]);
            $post = new NewsPost($row["id"], $row["title"], $row["content"], $row["postdate"], $row["lastedited"], $writers);
            $posts[] = $post;
        }
        return $posts;
    }
}