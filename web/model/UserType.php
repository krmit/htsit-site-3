<?php

namespace model;

/**
 * Class UserType
 * Used as an enum for different types of users.
 */
abstract class UserType
{
    // Has full admin access to create, edit or delete accounts.
    const ADMIN = "ADMIN";
    // Can moderate content but has to access to accounts.
    const TEACHER = "TEACHER";
    // A user with no special access.
    const STUDENT = "STUDENT";
}