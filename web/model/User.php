<?php

namespace model;
use DateTime;

/**
 * Class User
 * Represents the basics of a user needed for a functional
 * login system. For more specific features, extend this class.
 * Like if you want special properties for a student or teacher.
 */
class User
{
    private $name;
    private $username;
    private $passhash;
    private $usertype;
    private $enabled;
    private $lastactive;

    /**
     * @param string $username The username for this user
     * @param string $passhash The hashed password for this user
     * @param string $name The name of the user
     * @param UserType $usertype The type of the user used to determine privileges
     * @param bool $enabled Determines if the user is enabled or not
     * @param DateTime $lastactive The date and time the user was last active
     */
    public function __construct($username, $passhash, $name, UserType $usertype, $enabled, $lastactive)
    {
        $this->username = $username;
        $this->passhash = $passhash;
        $this->name = $name;
        $this->type = $usertype;
        $this->enabled = $enabled;
        $this->lastactive = new DateTime($lastactive);
    }

    public function __toString()
    {
        return $this->usertype . " " . $this->name . "(" . $this->username . "), enabled = " . $this->enabled
        . ", last active = " . $this->lastactive->format(Util::DateTimeFormat);
    }

    /**
     * Get the name of the user, i.e. 'John Doe'
     * @return string The actual name of the user
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set a new name for the user
     * @param string $name The new name to set
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get the username that is used for logging in and identification.
     * This cannot be changed
     * @return string The user's username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Get the password hash of this user. Verify this using the PHP
     * function password_verify
     * @return string This user's password hash
     */
    public function getPassHash()
    {
        return $this->passhash;
    }

    /**
     * Update the password for this user by setting the new hashed verison of it
     * @param string $passhash The hashed version of the password
     */
    public function setPasshash($passhash)
    {
        $this->passhash = $passhash;
    }

    /**
     * Get which type of user this is to determine which parts of the system
     * it has access to.
     * @see UserType
     * @return UserType the type of this user
     */
    public function getUsertype()
    {
        return $this->usertype;
    }

    /**
     * Sets the type of this user
     * @param UserType $usertype The new user type
     */
    public function setUsertype($usertype)
    {
        $this->usertype = $usertype;
    }

    /**
     * Get if this user is enabled or not. If it is not enabled,
     * the user will not be able to log in.
     * @return bool If the user is enabled
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Sets if the user is enabled or not. If it is not enabled,
     * the user will not be able to log in.
     * @param bool $enabled If the user should be enabled or not
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * Get the date and time when the user was last active.
     * @return DateTime Date and time last active
     */
    public function getLastactive()
    {
        return $this->lastactive;
    }

    /**
     * Set when the user was last active
     * @param DateTime $lastactive Date and time when last active
     */
    public function setLastactive($lastactive)
    {
        $this->lastactive = $lastactive;
    }
}