<?php

namespace model;

use PDO;

/**
 * Class Database
 * Handles the details regarding the connection to the MySQL database
 * used for this system.
 */
final class Database
{
    const HOSTNAME = "localhost";
    const DATABASE_NAME = "htsit";
    const USERNAME = "root";
    const PASSWORD = "password";
    const CHARSET = "utf8";

    const TABLE_NEWS = "htsit_news";
    const TABLE_USERS = "htsit_users";

    public static function getDB()
    {
        return new PDO('mysql:host=' . Database::HOSTNAME .
            ';dbname=' . Database::DATABASE_NAME .
            ';charset=' . Database::CHARSET,
            Database::USERNAME,
            Database::PASSWORD);
    }
}