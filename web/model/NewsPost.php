<?php

namespace model;

use DateTime;

class NewsPost
{
    private $id;
    private $title;
    private $content;
    private $postdate;
    private $lastEditedDate;
    private $writers;

    public function __construct($id, $title, $content, $postdate = "now", $lastedited = "now", array $writers)
    {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->postdate = new DateTime($postdate);
        $this->lastEditedDate = new DateTime($lastedited);
        $this->writers = $writers;
    }

    /**
     * @return string Example: "Hello World - 1970-01-01 00:00"
     */
    public function __toString()
    {
        return $this->title . " - " . $this->postdate->format(Util::DateTimeFormat);
    }

    /**
     * Get an id the uniquely identifies the post
     * @return string The posts database id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the title of this post
     * @return string Post title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the title of this post
     * @param string $title New post title
     */
    public function setTitle($title)
    {
        $this->title = $title;
        $this->lastEditedDate = new DateTime();
    }

    /**
     * Get the contents of this post
     * @return string Post content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set the contents of this post
     * @param string $content New post content
     */
    public function setContent($content)
    {
        $this->content = $content;
        $this->lastEditedDate = new DateTime();
    }

    /**
     * Get the date and time that this post was created
     * @return DateTime Post date and time
     */
    public function getPostdate()
    {
        return $this->postdate;
    }

    /**
     * Get the date and time when this post was last edited
     * @return DateTime Last edited date and time
     */
    public function getLastEditedDate()
    {
        return $this->lastEditedDate;
    }

    /**
     * Get all writers of this post, can be multiple
     * if someone else than the original writer edited the post
     * @return array The writers of this post
     */
    public function getWriters()
    {
        return $this->writers;
    }

    /**
     * Add a writer to this post
     * @param array $writer New post writer
     */
    public function addWriter($writer)
    {
        $exists = false;

        foreach ($this->writers as $val)
        {
            if ($val == $writer)
            {
                $exists = true;
            }
        }

        if (!$exists)
        {
            $this->writers[] = $writer;
        }
    }
}