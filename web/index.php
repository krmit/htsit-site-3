<!DOCTYPE html>
<html lang="sv">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>HTS IT - Informations- & Medieteknik på HTS</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

    <!-- SEO -->
    <meta name="description" content="HTS IT är teknikprogrammet med inriktning informations- & medieteknik på Hässleholms Tekniska Skola.
    Här kan du läsa mer om oss.">
    <meta property="og:description" content="HTS IT är teknikprogrammet med inriktning informations- & medieteknik på Hässleholms Tekniska Skola.
    Här kan du läsa mer om oss." />
    <meta property="og:title" content="HTS IT" />
    <meta property="og:type" content="website" />
</head>
<body>
<header>
    <div id="top-header" class="parallax-container">
        <nav class="transparent z-depth-0" role="navigation">
            <?php include_once('modules/nav-content.php') ?>
        </nav>
        <div class="section">
            <div class="container">
                <div class="row white-text center">
                    <br /><br />
                    <h1 class="header col s12 light">Lär dig om modern IT- och Datateknik vid Hässleholms Tekniska Skola</h1>
                    <h5 class="header col s12 light">Programmet för dig som är intresserad av datorer, programmering och/eller elektronik</h5>
                </div>
            </div>
        </div>
        <div class="parallax"><img src="img/hts.jpg" alt=""></div>
    </div>
</header>
<main>
    <div class="container">
        <div class="row">
            <div class="col s12 m8">
                <div class="section">
                    <h3 class="light">Nyheter</h3>
                    <div class="card-panel">
                        <h5>Välkommen till vår nya hemsida</h5>
                        <h5><small>2015-08-11</small></h5>
                        <p>
                            Som du säkert märker har vi uppdaterat designen på hemsidan! Till vår hjälp har vi
                            använt oss utav CSS ramverket <a href="http://materializecss.com" target="_blank">Materialize CSS</a>.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col s12 m4">
                <div class="section">
                    <?php include_once('modules/quicklinks.php'); ?>
                </div>
                <div class="section">
                    <a class="twitter-timeline"  href="https://twitter.com/htsit3040" data-widget-id="630918607858495488">Tweets by @htsit3040</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include_once('modules/footer.php'); ?>

<!--  Scripts-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/init.js"></script>

</body>
</html>
