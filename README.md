HTS IT 3.0
==========
This is the third revision of the HTS IT website available at
[htsit.se](http://htsit.se/).

This version attempts to bring a more modern design in the form
of Material Design as well as some more school related features.